public class TesController {
    public Contact c;
    public string accName{get; set;}
    
    public TesController(ApexPages.Standardcontroller con){
        this.c = (Contact)con.getRecord();
        accName = [Select AccountId, Name From Contact Where Id =: c.Id].AccountId;
    }
}