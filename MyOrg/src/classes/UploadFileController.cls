public with sharing class UploadFileController {
 
Public Id PageId {get;set;}
public Blob AttchBody {get;set;}
public String AttchDesc {get;set;}
public String AttchName {get;set;}
Public Attachment attch {get;set;}
 
public UploadFileController(){
 
PageId = ApexPages.CurrentPage().getParameters().get('Id');
 
}
 
public PageReference Upload(){
 
attch = new Attachment(ParentId=PageId,Description=AttchDesc,Name=AttchName,Body=AttchBody);
 
try{
insert attch;
AttchBody = null;
AttchDesc = null;
AttchName = null;
}
catch(Exception ex){
ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getlineNumber()+' '+ex.getMessage()));
return null;
 
}
ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
return null;
}
 
public PageReference UploadChatterFile() {
 
 
ContentWorkspace cw =[SELECT Id FROM ContentWorkspace WHERE DeveloperName = 'Test_Workspace'];
 
ContentVersion v = new ContentVersion();
v.versionData = AttchBody;
v.title = AttchName;
v.FirstPublishLocationId = cw.Id;
v.pathOnClient = AttchName;
 
If(PageId.getSObjectType() == Account.sObjectType){
v.Account__c = PageId;
}
If(PageId.getSObjectType() == Lead.sObjectType){
v.Lead__c = PageId;
}
 
v.Description = AttchDesc;
 
try{
insert v;
AttchBody = null;
AttchDesc = null;
AttchName = null;
}
catch(Exception ex){
ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getlineNumber()+' '+ex.getMessage()));
return null;
}
 
ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully in Chatter'));
return null;
}
}