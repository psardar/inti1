public with sharing class pageController {

public pageController(){
    users = new List<UserWrapper>();

    for(User theUser : [Select Id from User Where Name != null]){
        UserWrapper uw = new UserWrapper();
        uw.u = theUser;
        uw.selected = false;
        users.add(uw);

    }
}

public Boolean showPopup {get;private set;}

public List<UserWrapper> users {get;set;}
public class UserWrapper {
    public User u {get;private set;}
    public Boolean selected {get;set;}
}
}